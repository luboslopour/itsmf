<?php get_header() ?>
	<div class="py-5">
		<div class="container">
			<div class="lead">
				<p><?php _e('Na této stránce se nepodařilo vše tak, jak jste očekávali. Je možné že máte starý odkaz nebo došlo k jiné chybě.',get_template()); ?></p>
			</div>
			<div class="bg-light my-5 p-5 d-flex flex-wrap justify-content-center">
				<h2 class="mr-3"><?php _ex( 'Search for:', 'label' ) ?></h2>
				<?php get_search_form(); ?>
			</div>
		</div>
	</div>
<?php get_footer() ?>
