<?php

// libs
require('inc/languages.php');
require('inc/the_content.php');
require('inc/the_excerpt.php');
require('inc/images.php');
require('inc/gallery.php');
//require('inc/remove_post_types.php');
require('inc/wp_head.php');
require('inc/wp_scripts.php');
require('inc/wp_styles.php');
require('inc/nav_menu.php');
require('inc/class-wp-bootstrap-navwalker.php');
require('inc/bs4_pagination.php');
require('inc/yoast_seo.php');
require('inc/acf_google_key.php');
require('inc/breadcrumb_navxt.php');
require('inc/guttenberg_blocks.php');
require('inc/file_get_contents_curl.php');
require('inc/blocks.php');



// Custom Post Types
require('cpt/events.php');
require('cpt/partner.php');
require('cpt/subscribers.php');



// Blocks
require('blocks/accordion/functions.php');
require('blocks/slideshow/functions.php');
require('blocks/posts/functions.php');
require('blocks/events/functions.php');



// Advanced custom fields
require('acf/theme_options.php');
