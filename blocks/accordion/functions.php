<?php

function register_acf_block_accordion() {
    acf_register_block_type(array(
        'name'              => 'block-accordion',
        'title'             => _x('Rozklikávací obsah','admin',get_template()),
        'description'       => _x('Rozklikávací obsah.','admin',get_template()),
        'category'          => 'widgets',
        'render_template'   => 'blocks/accordion/accordion.php',
        'icon'              => 'list-view',
        'keywords'          => array('accordion'),
        'post_types'        => array('page'),
    ));
}

if( function_exists('acf_register_block_type') ) {
    add_action('acf/init', 'register_acf_block_accordion');
}




// ACF
if(function_exists('acf_add_local_field_group')) {
    acf_add_local_field_group(array(
        'key' => 'metabox-block-accordion',
        'title' => _x('Rozklikávací obsah','admin',get_template()),
        'location' => array (
            array (
                array (
                    'param' => 'block',
                    'operator' => '==',
                    'value' => 'acf/block-accordion',
                ),
            ),
        ),
        'fields' => array (
            array (
                'key' => 'accordion',
                'name' => 'accordion',
                'label' => _x('Rozklikávací obsah','admin',get_template()),
                'type' => 'repeater',
                'layout' => 'block',
                'sub_fields' => array(
                    array(
                        'key' => 'title',
                        'name' => 'title',
                        'label' => _x('Nadpis','admin','theme'),
                        'type' => 'text',
                    ),
                    array(
                        'key' => 'content',
                        'name' => 'content',
                        'label' => _x('Obsah','admin','theme'),
                        'type' => 'wysiwyg',
                    ),
                ),
            ),
        ),
    ));
}
