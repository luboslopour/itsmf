<?php
	$id = $block['id'];
	if( !empty($block['anchor']) ) {
		$id = $block['anchor'];
	}

	$className = 'block-accordion';
	if( !empty($block['className']) ) {
		$className .= ' ' . $block['className'];
	}
	if( !empty($block['align']) ) {
		$className .= ' align' . $block['align'];
	}

?>
<div id="<?php echo $id ?>" class="accordion <?php echo $className ?>">
	<?php if(have_rows('accordion')): ?>
		<?php $i=0; while(have_rows('accordion')): the_row(); ?>
			<div class="mb-2">
				<div id="heading-<?php echo $id.$i ?>">
					<button class="btn btn-info btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#<?php echo $id.$i ?>" aria-expanded="false" aria-controls="<?php echo $id.$i ?>">
						<?php the_sub_field('title'); ?>
					</button>
				</div>
				<div id="<?php echo $id.$i ?>" class="collapse" aria-labelledby="heading-<?php echo $id.$i ?>" data-parent="#<?php echo $id ?>">
					<div class="border p-3">
						<?php the_sub_field('content'); ?>
					</div>
				</div>
			</div>
		<?php $i++; endwhile; ?>
	<?php endif; ?>
</div>