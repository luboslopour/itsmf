<?php

$id = 'block-slideshow-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

$className = 'block-slideshow';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

?>

<?php if(have_rows('slideshow')): ?>
	<div id="<?php echo esc_attr($id); ?>" class="header-slider skewed-arrow-down mb-5 <?php echo esc_attr($className); ?>">
		<?php while(have_rows('slideshow')): the_row(); ?>
			<div class="item">
				<div class="embed-responsive">
					<?php echo wp_get_attachment_image(get_sub_field('slideshow_image'),'full',false,array('class'=>'embed-responsive-item'));  ?>
					<div class="container position-relative d-flex flex-column justify-content-center py-5 min-vh-75">
						<div class="row mb-5 mb-md-0">
							<div class="col-md-9 col-lg-8 col-xl-7">
								<div class="embed-responsive">
									<div class="bg-white opacity-75 embed-responsive-item"></div>
									<div class="position-relative p-5">
										<h2 class="h3"><?php the_sub_field('slideshow_title'); ?></h2>
										<p><?php the_sub_field('slideshow_content'); ?></p>
										<a class="btn btn-primary" target="<?php echo get_sub_field('slideshow_link')['target'] ?>" href="<?php echo get_sub_field('slideshow_link')['url'] ?>">
											<?php echo get_sub_field('slideshow_link')['title'] ?>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php endwhile; ?>
	</div>
<?php else: ?>
	<?php if(is_admin()) echo '<p>'._x('Přidejte položku pro slideshow.','admin',get_template()).'</p>'; ?>
<?php endif; ?>


<?php if(is_admin()): ?>
<style>
	#<?php echo esc_attr($id); ?> .embed-responsive {
		display: flex;
		align-items: center;
		margin-bottom: 1rem;
		background: #f1f1f1;
	}
	#<?php echo esc_attr($id); ?> .embed-responsive-item {
		max-width: 25%;
		height: auto;
		margin-right: 1rem;
	}
	#<?php echo esc_attr($id); ?> .container {
		
	}
</style>
<?php endif; ?>