<?php

function register_acf_block_types() {
    acf_register_block_type(array(
        'name'              => 'block-slideshow',
        'title'             => _x('Slideshow','admin',get_template()),
        'description'       => _x('Vytvořit slideshow.','admin',get_template()),
        'category'          => 'widgets',
        'render_template'   => 'blocks/slideshow/slideshow.php',
        'icon'              => 'welcome-view-site',
        'keywords'          => array('slideshow'),
        'post_types'        => array('page'),
        'supports'          => array(
            'align'             => array('full'),
        ),
        'align' => 'full',
    ));
}

if( function_exists('acf_register_block_type') ) {
    add_action('acf/init', 'register_acf_block_types');
}




// ACF
if(function_exists('acf_add_local_field_group')) {
    acf_add_local_field_group(array(
        'key' => 'metabox-category-posts',
        'title' => _x('Nastavení','admin','theme'),
        'location' => array (
            array (
                array (
                    'param' => 'block',
                    'operator' => '==',
                    'value' => 'acf/block-slideshow',
                ),
            ),
        ),
        'fields' => array (
            array (
                'key' => 'slideshow',
                'name' => 'slideshow',
                'label' => _x('Slideshow','admin',get_template()),
                'type' => 'repeater',
                'layout' => 'block',
                'required' => true,
                'sub_fields' => array(
                    array(
                        'key' => 'slideshow_image',
                        'name' => 'slideshow_image',
                        'label' => _x('Obrázek','admin','theme'),
                        'type' => 'image',
                        'preview_size' => 'thumbnail',
                        'return_format' => 'id',
                        'required' => true,
                        'min_width' => '1920',
                        'min_height' => '740',
                    ),
                    array(
                        'key' => 'slideshow_title',
                        'name' => 'slideshow_title',
                        'label' => _x('Nadpis','admin','theme'),
                        'type' => 'text',
                        'new_lines' => 'br',
                    ),
                    array(
                        'key' => 'slideshow_content',
                        'name' => 'slideshow_content',
                        'label' => _x('Text','admin','theme'),
                        'type' => 'textarea',
                        'new_lines' => 'br',
                    ),
                    array(
                        'key' => 'slideshow_link',
                        'name' => 'slideshow_link',
                        'label' => _x('Tlačítko','admin','theme'),
                        'type' => 'link',
                    ),
                ),
            ),
        ),
    ));
}
