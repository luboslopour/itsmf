<?php

function register_acf_block_events() {
    acf_register_block_type(array(
        'name'              => 'block-events',
        'title'             => _x('Nejbližší akce','admin',get_template()),
        'description'       => _x('Nejbližší akce.','admin',get_template()),
        'category'          => 'widgets',
        'render_template'   => 'blocks/events/events.php',
        'icon'              => 'calendar-alt',
        'keywords'          => array('events'),
        'post_types'        => array('page'),
        'align'             => false,
    ));
}

if( function_exists('acf_register_block_type') ) {
    add_action('acf/init', 'register_acf_block_events');
}




// ACF
if(function_exists('acf_add_local_field_group')) {
    acf_add_local_field_group(array(
        'key' => 'metabox-block-events',
        'title' => _x('Nejbližší akce','admin',get_template()),
        'location' => array (
            array (
                array (
                    'param' => 'block',
                    'operator' => '==',
                    'value' => 'acf/block-events',
                ),
            ),
        ),
        'fields' => array (
            array(
                'key' => 'posts_per_page',
                'name' => 'posts_per_page',
                'label' => _x('Počet příspěvků','admin','theme'),
                'type' => 'number',
                'default_value' => 3,
                'min' => 1,
            ),
            array (
                'key' => 'theme-color',
                'name' => 'theme-color',
                'label' => _x('Barva boxu','admin','theme'),
                'type' => 'select',
                'choices' => array(
                    'primary' => _x('Červená','admin','theme'),
                    'info' => _x('Modrá','admin','theme'),
                    'success' => _x('Zelená','admin','theme'),
                    'warning' => _x('Žlutá','admin','theme'),
                    'danger' => _x('Fialová','admin','theme'),
                    'secondary' => _x('Šedá','admin','theme'),
                ),
                'default_value'  => array('primary'),
            ),
        ),
    ));
}
