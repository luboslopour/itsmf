<?php
	$id = $block['id'];
	if( !empty($block['anchor']) ) {
		$id = $block['anchor'];
	}

	$className = 'block-accordion';
	if( !empty($block['className']) ) {
		$className .= ' ' . $block['className'];
	}
	if( !empty($block['align']) ) {
		$className .= ' align' . $block['align'];
	}

?>

<?php

$id = 'block-events-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

$className = 'block-events';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

?>

<div id="<?php echo $id ?>" class="<?php echo $className ?>">
	<?php
		query_posts(array(
			'post_type' => 'events',
			'posts_per_page' => get_field('posts_per_page'),
			'meta_key'  => 'date_from',
			'orderby'   => 'meta_value_num',
			'order'	 => 'DESC',

			/*
			'meta_query'	=> array(
				array(
					'key'	  	=> 'date_from',
					'value'	  	=> date('Ymd'),
					'type'	  => 'DATE',
					'compare' 	=> '>=',
				),
			),
			
			*/
		));

	?>
	<?php if(have_posts()): ?>
		<section>
			<h2 class="h4"><?php _e('Nejbližší akce','theme') ?></h2>
			<div class="w-25 border border-<?php the_field('theme-color') ?> mt-3"></div>
			<?php while(have_posts()): the_post(); ?>
				<article class="border-bottom border-light pt-3 pb-3">
					<h3 class="h6 mb-0"><a class="text-<?php the_field('theme-color') ?>" href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
					<p class="text-muted mb-0">
						<svg class="icon-material-calendar mb-1 mr-1">
							<use xlink:href="#icon-material-calendar"></use>
						</svg>
						<small><?php the_time('j. n. Y') ?></small>
					</p>
				</article>
			<?php endwhile; ?>
			<a href="<?php echo get_post_type_archive_link('events'); ?>" class="btn btn-<?php the_field('theme-color') ?> mt-4">
				<?php _e('Zobrazit vše','theme') ?>
			</a>
		</section>
	<?php else: ?>
		<?php
			if(is_admin()){
				_ex('Vyberte kategorii příspěvku.',get_template());
			} else {
				_ex('Nebyl publikovaný žádný příspěvek.',get_template());
			}
		?>
	<?php endif; ?>
	<?php wp_reset_query(); ?>
</div>

<?php if(is_admin()): ?>
<style>
	#<?php echo esc_attr($id); ?> {
		font-size: .75rem;
	}
	#<?php echo esc_attr($id); ?> svg {
		display: none;
	}
	#<?php echo esc_attr($id); ?> h2 {
		font-size: 1rem;
	}
	#<?php echo esc_attr($id); ?> h3 {
		font-size: .75rem;
	}
	#<?php echo esc_attr($id); ?> p {
		margin: 0 0 1rem;
	}
</style>
<?php endif; ?>