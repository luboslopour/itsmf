<?php

function register_acf_block_posts() {
    acf_register_block_type(array(
        'name'              => 'block-posts',
        'title'             => _x('Výpis příspěvků','admin',get_template()),
        'description'       => _x('Výpis příspěvků.','admin',get_template()),
        'category'          => 'widgets',
        'render_template'   => 'blocks/posts/posts.php',
        'icon'              => 'admin-post',
        'keywords'          => array('posts'),
        'post_types'        => array('page'),
        'align'             => false,
    ));
}

if( function_exists('acf_register_block_type') ) {
    add_action('acf/init', 'register_acf_block_posts');
}




// ACF
if(function_exists('acf_add_local_field_group')) {
    acf_add_local_field_group(array(
        'key' => 'metabox-block-posts',
        'title' => _x('Výpis příspěvků','admin',get_template()),
        'location' => array (
            array (
                array (
                    'param' => 'block',
                    'operator' => '==',
                    'value' => 'acf/block-posts',
                ),
            ),
        ),
        'fields' => array (
            array(
                'key' => 'posts_per_page',
                'name' => 'posts_per_page',
                'label' => _x('Počet příspěvků','admin','theme'),
                'type' => 'number',
                'default_value' => 3,
                'min' => 1,
            ),
            array (
                'key' => 'orderby',
                'name' => 'orderby',
                'label' => _x('Seředit dle','admin','theme'),
                'type' => 'select',
                'choices' => array(
                    'date' => _x('Datum','admin','theme'),
                    'title' => _x('Nadpis','admin','theme'),
                    'rand' => _x('Náhodně','admin','theme'),
                ),
                'default_value'  => array('date'),
                'allow_null' => false,
            ),
            array (
                'key' => 'order',
                'name' => 'order',
                'label' => _x('Řazení','admin','theme'),
                'type' => 'select',
                'choices' => array(
                    'ASC' => _x('Sestupně','admin','theme'),
                    'DESC' => _x('Vzestupně','admin','theme'),
                ),
                'default_value'  => array('DESC'),
                'allow_null' => false,
            ),
            array (
                'key' => 'terms',
                'name' => 'terms',
                'label' => _x('Kategorie','admin','theme'),
                'type' => 'taxonomy',
                'taxonomy' => 'category',
                'field_type' => 'select',
                'allow_null' => false,
                'required' => true,
            ),
            array (
                'key' => 'theme-color',
                'name' => 'theme-color',
                'label' => _x('Barva boxu','admin','theme'),
                'type' => 'select',
                'choices' => array(
                    'primary' => _x('Červená','admin','theme'),
                    'info' => _x('Modrá','admin','theme'),
                    'success' => _x('Zelená','admin','theme'),
                    'warning' => _x('Žlutá','admin','theme'),
                    'danger' => _x('Fialová','admin','theme'),
                    'secondary' => _x('Šedá','admin','theme'),
                ),
                'default_value'  => array('primary'),
            ),
        ),
    ));
}
