<?php

require_once dirname( __FILE__ ) . '/' . '../../../../wp-load.php';

if(current_user_can('edit_pages')) {
	header('Content-Type: text/csv; charset=utf-8');
	header('Content-Disposition: attachment; filename=participiants-'.time().'.csv');
	$output = fopen('php://output','w');

	echo 'Last Name,First Name,E-mail'."\n";
	
	if(have_rows('participants',$_GET['id'])) {
		while(have_rows('participants',$_GET['id'])) {
			the_row();
			fputcsv($output,array(
				get_sub_field('participants_lastname'),
				get_sub_field('participants_firstname'),
				get_sub_field('participants_email'),
			));
		}
	}

} else {
	wp_safe_redirect(admin_url());
	exit;
}
