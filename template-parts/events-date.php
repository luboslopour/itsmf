<?php

$dateFrom = strtotime(get_field('date_from'));
$dateTo   = strtotime(get_field('date_to'));
$date     = date('j. n. Y',$dateFrom);

if(!get_field('multiday') == 1) {
	$date = date('j. n. Y',$dateFrom);
	if(!get_field('fullday') == 1) {
		$date = $date.' ('.get_field('time_from').' - '.get_field('time_to').')';
	}
} else {
	if($dateTo) {
		$date = date('j. n. Y',$dateFrom).' - '.date('j. n. Y',$dateTo);
		if(date('Y',$dateFrom) == date('Y',$dateTo)) {
			$date = date('j. n.',$dateFrom).' - '.date('j. n. Y',$dateTo);
			if(date('n',$dateFrom) == date('n',$dateTo)) {
				$date = date('j.',$dateFrom).' - '.date('j. n. Y',$dateTo);
			}
		}
	}	
}