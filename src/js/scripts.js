// --- TOOLTIP ---
$('[data-toggle=tooltip]').tooltip();



// --- ANIMATE ---
$('[data-animate=fadeInUp]').addClass('animate-hidden').viewportChecker({
	classToAdd: 'animate-visible animated fadeInUp',
	offset: 50,
	repeat: true,
});


// --- FANCYBOX ---
var image = 'a[href$=".gif"],a[href$=".jpg"],a[href$=".jpeg"],a[href$=".png"],a[href$=".svg"],a[href$=".GIF"],a[href$=".JPG"],a[href$=".JPEG"],a[href$=".PNG"],a[href$=".SVG"]';
$(image).attr('data-fancybox','');
$('.gallery').each(function(){
	id = $(this).attr('id');
	$(image,this).attr('data-fancybox',id); 
});
$('[data-fancybox]').fancybox({
	buttons : [
		'close'
	],
});

// --- OWL.CAROUSEL ---
$('.header-slider').each(function(){
	if($('.item',this).length>1) {
		$(this).addClass('owl-carousel').owlCarousel({
			loop: true,
			items: 1,
			nav: true,
			dots: true,
			margin: 0,
		})
	}
})

$('.partners-slider').each(function(){
	if($('.item',this).length>1) {
		$(this).addClass('owl-carousel').owlCarousel({
			loop: true,
			items: 1,
			nav: false,
			dots: true,
			margin: 0,
			autoplay: true,
			autoplayHoverPause: true,
			autoplayTimeout: 7000,
		})
	}
})