<form role="search" method="get" id="searchform" class="form-inline" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label for="s" class="sr-only"><?php _ex( 'Search for:', 'label' ) ?></label>
	<div class="input-group">
		<input type="text" class="form-control" value="<?php echo get_search_query(); ?>" name="s" id="s" placeholder="<?php _ex('Search &hellip;','placeholder') ?>">
		<div class="input-group-append">
			<input type="submit" class="btn btn-primary" id="searchsubmit" value="<?php _ex('Search','submit button') ?>" />
		</div>
	</div>
</form>