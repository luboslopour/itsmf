<?php

function register_cpt_subscribers() {
	$labels = array(
		'name'                => _x( 'Odběratelé','admin',get_template()),
		'singular_name'       => _x( 'Odběratelé','admin singular name', get_template() ),
		'menu_name'           => _x( 'Odběratelé','admin',get_template() ),
		'name_admin_bar'      => _x( 'Odběratelé','admin',get_template()).' - '.strtolower(__( 'New Post')),
		'all_items'           => _x( 'Všivhni odběratelé','admin',get_template() ), // or 'All Pages'
		'add_new'             => _x( 'Přidat odběratele','admin',get_template()), // or 'Add New', 'page'
		'add_new_item'        => _x( 'Přidat odběratele','admin',get_template()), // or 'Add New Page'
		'edit_item'           => _x( 'Upravit odběratele','admin',get_template()), // or 'Edit Page'
		'new_item'            => _x( 'Nový odběratel','admin',get_template()), // or 'New Page'
		'view_item'           => _x( 'Přehled odběratelů','admin',get_template()), // or 'View Page'
		'search_items'        => _x( 'Hledat odběratele','admin',get_template()), // or 'Search Pages'
		'not_found'           => _x( 'Nebyly nalezeni žádní odběratelé','admin',get_template()), // or 'No pages found.'
		'not_found_in_trash'  => _x( 'V koši nebyly nalezeni žádní odběratelé.','admin',get_template()), // or 'No pages found in Trash.'
	);
	$args = array(
		'supports'            => array('title'),
		'menu_icon'           => 'dashicons-email',
		'menu_position'       => 25,
		'labels'              => $labels,
		'hierarchical'        => false,
		'public'              => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => false,
		'publicly_queryable'  => false,
		'exclude_from_search' => true,
		'has_archive'         => false,
		'query_var'           => false,
		'can_export'          => true,
		'capability_type'     => 'page'
	);
	register_post_type( 'subscribers', $args );
}
add_action( 'init', 'register_cpt_subscribers', 0 );

// Post Thumbnails
add_theme_support('post-thumbnails', array('subscribers'));

// admin order
if (is_admin()) {
	function cpt_subscribers_order($wp_query) {
		$post_type = $wp_query->query['post_type'];
		if ( $post_type == 'subscribers') {
			$wp_query->set('orderby', 'title');
			$wp_query->set('order', 'ASC');
		}
	}
	add_filter('pre_get_posts', 'cpt_subscribers_order');
}

// ACF
if(function_exists('acf_add_local_field_group')) {
	acf_add_local_field_group(array(
		'key' => 'meta-subscribers',
		'title' => _x('Přehled odběratele','admin',get_template()),
		'label_placement' => 'left',
		'style' => 'seamless',
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'subscribers',
				),
			),
		),
		'fields' => array (
			array (
				'key' => 'lastname',
				'name' => 'lastname',
				'label' => _x('Příjmení','admin',get_template()),
				'type' => 'text',
			),
			array (
				'key' => 'firstname',
				'name' => 'firstname',
				'label' => _x('Jméno','admin',get_template()),
				'type' => 'text',
			),
		),
	));
}


// ADMIN CSV LINK
function subscribers_admin_csv() {
    global $submenu;
    $url = get_bloginfo('template_url').'/csv/subscribers.php';
    $submenu['edit.php?post_type=subscribers'][] = array(_x('Exportovat jako CSV','admin',get_template()), 'edit_pages', $url);
}
add_action('admin_menu', 'subscribers_admin_csv');



// admin css
function subscribers_admin_css() {
  echo '<style>
	.post-type-subscribers .row-actions .inline,
	.post-type-subscribers #minor-publishing {
		display: none;
	}
  </style>';
}
add_action('admin_head', 'subscribers_admin_css');