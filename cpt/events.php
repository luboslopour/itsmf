<?php

function register_cpt_events() {
	$labels = array(
		'name'                => _x( 'Události', 'Post Type General Name', get_template() ),
		'singular_name'       => _x( 'Události', 'Post Type Singular Name', get_template() ),
		'menu_name'           => _x( 'Události', 'Post Type General Name', get_template() ),
		'name_admin_bar'      => _x( 'Události', 'Post Type General Name','add new from admin bar').' - '.strtolower(__( 'New Post')),
		'all_items'           => __( 'All Posts' ), // or 'All Pages'
		'add_new'             => _x( 'Add New', 'post'), // or 'Add New', 'page'
		'add_new_item'        => __( 'Add New Post'), // or 'Add New Page'
		'edit_item'           => __( 'Edit Post'), // or 'Edit Page'
		'new_item'            => __( 'New Post'), // or 'New Page'
		'view_item'           => __( 'View Post'), // or 'View Page'
		'search_items'        => __( 'Search Posts'), // or 'Search Pages'
		'not_found'           => __( 'No posts found.'), // or 'No pages found.'
		'not_found_in_trash'  => __( 'No posts found in Trash.'), // or 'No pages found in Trash.'
	);
	$rewrite = array(
	    'slug'                => _x( 'udalosti', 'Post Type Slug', get_template() ),
	);
	$args = array(
		'supports'            => array( 'title','editor','excerpt','thumbnail','revisions','page-attributes' ),
		'menu_icon'           => 'dashicons-calendar-alt',
		'menu_position'       => 5,
		'labels'              => $labels,
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'page'
	);
	register_post_type( 'events', $args );
}
add_action( 'init', 'register_cpt_events', 0 );

// Post Thumbnails
add_theme_support('post-thumbnails', array('events'));

// admin order
if (is_admin()) {
	function cpt_events_order($wp_query) {
		$post_type = $wp_query->query['post_type'];
		if ( $post_type == 'events') {
			$wp_query->set('orderby', 'date');
			$wp_query->set('order', 'DESC');
		}
	}
	add_filter('pre_get_posts', 'cpt_events_order');
}



// ACF
if(function_exists('acf_add_local_field_group')) {
	acf_add_local_field_group(array(
		'key' => 'meta-event-date',
		'title' => __('Datum konání akce',get_template()),
		'label_placement' => 'left',
		'position' => 'acf_after_title',
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'events',
				),
			),
		),
		'fields' => array (
			array (
				'key' => 'date_from',
				'name' => 'date_from',
				'label' => __('Datum začátek',get_template()),
				'type' => 'date_picker',
				'display_format' => 'j. n. Y',
				'return_format' => 'Ymd',
				'first_day' => '1',
				'required' => true,
			),
			array (
				'key' => 'time_from',
				'name' => 'time_from',
				'label' => __('Čas začátek',get_template()),
				'type' => 'time_picker',
				'display_format' => 'H:i:s',
				'return_format' => 'H:i',
				'required' => true,
				'conditional_logic' => array(
					array(
						array(
							'field' => 'fullday',
							'operator' => '==',
							'value' => '',
						),
						array(
							'field' => 'multiday',
							'operator' => '==',
							'value' => '',
						)
					)
				),
			),
			array (
				'key' => 'date_to',
				'name' => 'date_to',
				'label' => __('Datum konec',get_template()),
				'type' => 'date_picker',
				'display_format' => 'j. n. Y',
				'return_format' => 'Ymd',
				'first_day' => '1',
				'conditional_logic' => array(
					array(
						array(
							'field' => 'multiday',
							'operator' => '==',
							'value' => '1',
						)
					)
				),
			),
			array (
				'key' => 'time_to',
				'name' => 'time_to',
				'label' => __('Čas konec',get_template()),
				'type' => 'time_picker',
				'display_format' => 'H:i:s',
				'return_format' => 'H:i',
				'required' => true,
				'conditional_logic' => array(
					array(
						array(
							'field' => 'fullday',
							'operator' => '==',
							'value' => '',
						),
						array(
							'field' => 'multiday',
							'operator' => '==',
							'value' => '',
						)
					)
				),
			),
			array (
				'key' => 'fullday',
				'name' => 'fullday',
				'label' => __('Celodenní akce',get_template()),
				'type' => 'true_false',
				'message' => __('celodenní akce',get_template()),
				'default_value' => 1,
				'conditional_logic' => array(
					array(
						array(
							'field' => 'multiday',
							'operator' => '==',
							'value' => '',
						)
					)
				),
			),
			array (
				'key' => 'multiday',
				'name' => 'multiday',
				'label' => __('Vícedenní akce',get_template()),
				'type' => 'true_false',
				'message' => __('vícedenní akce',get_template()),
				'default_value' => 0,
			),
		),
	));
}


// ACF location
if(function_exists('acf_add_local_field_group')) {
	acf_add_local_field_group(array(
		'key' => 'meta-event-location',
		'title' => __('Místo konání akce',get_template()),
		'label_placement' => 'left',
		'position' => 'acf_after_title',
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'events',
				),
			),
		),
		'fields' => array (
			array (
				'key' => 'location_name',
				'name' => 'location_name',
				'label' => __('Název místa konání',get_template()),
				'type' => 'text',
				'required' => true,
			),
			array (
				'key' => 'location_street',
				'name' => 'location_street',
				'label' => __('Ulice',get_template()),
				'type' => 'text',
				'required' => true,
			),
			array (
				'key' => 'location_city',
				'name' => 'location_city',
				'label' => __('Město',get_template()),
				'type' => 'text',
				'required' => true,
			),
			array (
				'key' => 'location_zip',
				'name' => 'location_zip',
				'label' => __('PSČ',get_template()),
				'type' => 'text',
				'required' => true,
			),
			array (
				'key' => 'location_country',
				'name' => 'location_country',
				'label' => __('Země',get_template()),
				'type' => 'text',
			),
		),
	));
}


if(function_exists('acf_add_local_field_group')) {
	acf_add_local_field_group(array(
		'key' => 'meta-event-participants',
		'title' => __('Účastnící',get_template()),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'events',
				),
			),
		),
		'fields' => array (
			array (
				'key' => 'participants_count',
				'name' => 'participants_count',
				'label' => __('Omezení počtu účastníků',get_template()),
				'type' => 'number',
				'min' => '0',
			),
			array (
				'key' => 'participants',
				'name' => 'participants',
				'label' => __('Účastníci',get_template()),
				'type' => 'repeater',
				'layout' => 'table',
				'button_label' => __('Přidat účastníka',get_template()),
				'sub_fields' => array(
					array (
						'key' => 'participants_lastname',
						'name' => 'participants_lastname',
						'label' => __('Příjmení',get_template()),
						'type' => 'text',
						'required' => true,
					),
					array (
						'key' => 'participants_firstname',
						'name' => 'participants_firstname',
						'label' => __('Jméno',get_template()),
						'type' => 'text',
						'required' => true,
					),
					array (
						'key' => 'participants_email',
						'name' => 'participants_email',
						'label' => __('E-mail',get_template()),
						'type' => 'email',
						'required' => true,
					),
				),
			),
			array (
				'key' => 'participants_export_button',
				'name' => 'participants_export_button',
				'label' => __('',get_template()),
				'type' => 'message',
				'message' => '<div class="acf-actions"><a class="button button-primary" target="_blank" href="'.get_template_directory_uri().'/xml/events-participants.php?id='.$_GET['post'].'">'.__('Stáhnout CSV s účastníky',get_template()).'</a></div>',
			),
		),
	));
}



if(function_exists('acf_add_local_field_group')) {
	acf_add_local_field_group(array(
		'key' => 'meta-event-poll',
		'title' => __('Anketa spokojenosti',get_template()),
		'menu_order' => 1,
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'events',
				),
			),
		),
		'fields' => array (
			array (
				'key' => 'poll',
				'name' => 'poll',
				'label' => __('Anketa',get_template()),
				'type' => 'repeater',
				'layout' => 'row',
				'button_label' => __('Přidat hlasování',get_template()),
				'sub_fields' => array(
					array (
						'key' => 'poll_1',
						'name' => 'poll_1',
						'label' => __('Zaujalo Vás téma semináře?',get_template()),
						'type' => 'select',
						'choices' => array(
							'1' => __('Velmi',get_template()),
							'2' => __('Spíše ano',get_template()),
							'3' => __('Spíše ne',get_template()),
						),
						'allow_null' => true,
					),
					array (
						'key' => 'poll_2',
						'name' => 'poll_2',
						'label' => __('Odpovídala náplň semináře Vašemu očekávání?',get_template()),
						'type' => 'select',
						'choices' => array(
							'1' => __('Velmi',get_template()),
							'2' => __('Spíše ano',get_template()),
							'3' => __('Spíše ne',get_template()),
						),
						'allow_null' => true,
					),
					array (
						'key' => 'poll_3',
						'name' => 'poll_3',
						'label' => __('Měl pro Vás seminář přínos?',get_template()),
						'type' => 'select',
						'choices' => array(
							'1' => __('Velmi',get_template()),
							'2' => __('Spíše ano',get_template()),
							'3' => __('Spíše ne',get_template()),
						),
						'allow_null' => true,
					),
					array (
						'key' => 'poll_4',
						'name' => 'poll_4',
						'label' => __('Uvítali byste další seminář na stejné téma či se stejnými řečníky?',get_template()),
						'type' => 'select',
						'choices' => array(
							'1' => __('Velmi',get_template()),
							'2' => __('Spíše ano',get_template()),
							'3' => __('Spíše ne',get_template()),
						),
						'allow_null' => true,
					),
					array (
						'key' => 'poll_5',
						'name' => 'poll_5',
						'label' => __('Jste spokojeni s úrovní celkové organizace itSM Prakticky?',get_template()),
						'type' => 'select',
						'choices' => array(
							'1' => __('Velmi',get_template()),
							'2' => __('Spíše ano',get_template()),
							'3' => __('Spíše ne',get_template()),
						),
						'allow_null' => true,
					),
					array (
						'key' => 'poll_6',
						'name' => 'poll_6',
						'label' => __('Byl termín zveřejnění informací o konání semináře vyhovující?',get_template()),
						'type' => 'select',
						'choices' => array(
							'1' => __('Velmi',get_template()),
							'2' => __('Spíše ano',get_template()),
							'3' => __('Spíše ne',get_template()),
						),
						'allow_null' => true,
					),
					array (
						'key' => 'poll_7',
						'name' => 'poll_7',
						'label' => __('Byly informace o semináři v pozvánce a na webu dostačující? Pokud ne, uveďte prosím proč.',get_template()),
						'type' => 'select',
						'choices' => array(
							'1' => __('Velmi',get_template()),
							'2' => __('Spíše ano',get_template()),
							'3' => __('Spíše ne',get_template()),
						),
						'allow_null' => true,
					),
					array (
						'key' => 'poll_8',
						'name' => 'poll_8',
						'label' => __('Byli jste spokojeni s občerstvením?',get_template()),
						'type' => 'select',
						'choices' => array(
							'1' => __('Velmi',get_template()),
							'2' => __('Spíše ano',get_template()),
							'3' => __('Spíše ne',get_template()),
						),
						'allow_null' => true,
					),
					array (
						'key' => 'poll_9',
						'name' => 'poll_9',
						'label' => __('Z jakého zdroje jste se o konání semináře dozvěděli.',get_template()),
						'type' => 'select',
						'choices' => array(
							'1' => __('email',get_template()),
							'2' => __('info od kolegy / známého',get_template()),
							'3' => __('www.itsmf.cz',get_template()),
						),
						'allow_null' => true,
					),
					array (
						'key' => 'poll_topics',
						'name' => 'poll_topics',
						'label' => __('Jaké téma doporučujete pro další semináře?',get_template()),
						'type' => 'textarea',
						'new_lines' => 'br',
						'rows' => 2,
					),
					array (
						'key' => 'poll_message',
						'name' => 'poll_message',
						'label' => __('Vaše případné další komentáře',get_template()),
						'type' => 'textarea',
						'new_lines' => 'br',
						'rows' => 2,
					),
				),
			),
			array (
				'key' => 'poll_export_button',
				'name' => 'poll_export_button',
				'label' => __('',get_template()),
				'type' => 'message',
				'message' => '<div class="acf-actions"><a class="button button-primary" target="_blank" href="'.get_template_directory_uri().'/charts/events-questionnaire.php?id='.$_GET['post'].'">'.__('Zobrazit výsledky dotazníku spokojenosti',get_template()).'</a></div>',
			),
		),
	));
}



if(function_exists('acf_add_local_field_group')) {
	acf_add_local_field_group(array(
		'key' => 'meta-event-forms',
		'title' => __('Formuláře',get_template()),
		'position' => 'side',
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'events',
				),
			),
		),
		'fields' => array (
			array (
				'key' => 'forms',
				'name' => 'forms',
				'label' => '',
				'type' => 'checkbox',
				'choices' => array(
					'registration' => __('Registrační formulář pro přihlášené',get_template()),
					'questionnaire' => __('Dotazník spokojenosti pro přihlášené',get_template()),
				),
			),
		),
	));
}


/*
function the_content_events($content) {
		$ac='';
		$bc='';

	if(is_singular('events')) {
		// before the_content
		$bc.= '<div class="row">';
			$bc.='<div class="col-md-6">';
				$bc.='<h2 class="h3">'.__('Datum konání').':</h2>';
				$bc.='<p>';
					if(get_field('date_from')) $bc.='<strong>'.get_field('date_from').'</strong>';
					if(get_field('time_from') && get_field('fullday')==0) $bc.=' <small>'.get_field('time_from').'</small>';
					if(get_field('time_to') && get_field('fullday')==0 or get_field('date_to') && get_field('multiday')==1) $bc.=' &rarr; ';
					if(get_field('date_to') && get_field('multiday')==1) $bc.='<strong>'.get_field('date_to').'</strong>';
					if(get_field('time_to') && get_field('fullday')==0) $bc.=' <small>'.get_field('time_to').'</small>';
				$bc.='</p>';
			$bc.='</div>';
			$bc.='<div class="col-md-6">';
					$bc.='<h2 class="h3">'.__('Místo konání').':</h2>';
					$bc.='<address>';
					$bc.='<strong>'.get_field('location_name').'</strong>';
					$bc.='<br>'.get_field('location_street');
					$bc.='<br>'.get_field('location_zip');
					$bc.=' '.get_field('location_city');
					if(get_field('location_country')) $bc.='<br>'.get_field('location_country');
					$bc.='</address>';
			$bc.='</div>';
		$bc.='</div>';	
	}
		echo $bc.$content.$ac;
		if(is_user_logged_in()){
			if(in_array('registration',get_field('forms')))  { echo'<hr>'; get_template_part('forms/registration/form'); }
			if(in_array('questionnaire',get_field('forms'))) {  echo'<hr>'; get_template_part('forms/poll/form'); }
		}
}

add_filter( 'the_content', 'the_content_events', 20 );
*/







// admin css
function events_admin_css() {
  echo '<style>
	.acf-field-poll {
		display: none;
	}
  </style>';
}
add_action('admin_head', 'events_admin_css');