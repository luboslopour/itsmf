<?php

function register_cpt_partner() {
	$labels = array(
		'name'                => _x( 'Partneři', 'Post Type General Name', 'theme' ),
		'singular_name'       => _x( 'Partneři', 'Post Type Singular Name', 'theme' ),
		'menu_name'           => _x( 'Partneři', 'Post Type General Name', 'theme' ),
		'name_admin_bar'      => _x( 'Partneři', 'Post Type General Name','add new from admin bar').' - '.strtolower(__( 'New Post')),
		'all_items'           => __( 'All Posts' ), // or 'All Pages'
		'add_new'             => _x( 'Add New', 'post'), // or 'Add New', 'page'
		'add_new_item'        => __( 'Add New Post'), // or 'Add New Page'
		'edit_item'           => __( 'Edit Post'), // or 'Edit Page'
		'new_item'            => __( 'New Post'), // or 'New Page'
		'view_item'           => __( 'View Post'), // or 'View Page'
		'search_items'        => __( 'Search Posts'), // or 'Search Pages'
		'not_found'           => __( 'No posts found.'), // or 'No pages found.'
		'not_found_in_trash'  => __( 'No posts found in Trash.'), // or 'No pages found in Trash.'
	);
	$rewrite = array(
	    'slug'                => _x( 'partner', 'Post Type Slug', 'theme' ),
	);
	$args = array(
		'supports'            => array( 'title','thumbnail','page-attributes' ),
		'menu_icon'           => 'dashicons-groups',
		'menu_position'       => 25,
		'labels'              => $labels,
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => false,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => false,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'page'
	);
	register_post_type( 'partner', $args );
}
add_action( 'init', 'register_cpt_partner', 0 );

// Post Thumbnails
add_theme_support('post-thumbnails', array('partner'));

// admin order
if (is_admin()) {
	function cpt_partner_order($wp_query) {
		$post_type = $wp_query->query['post_type'];
		if ( $post_type == 'partner') {
			$wp_query->set('orderby', 'menu_order title');
			$wp_query->set('order', 'ASC');
		}
	}
	add_filter('pre_get_posts', 'cpt_partner_order');
}


// register taxonomy
function create_partner_category_taxonomies() {
	$labels = array(
		'name'              => _x( 'Kategorie partnerů', 'taxonomy general name', 'theme' ),
		'singular_name'     => _x( 'Kategorie partnerů', 'taxonomy singular name', 'theme' ),
		'menu_name'         => __( 'Kategorie partnerů', 'theme' ),
	);
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'partner-category', 'hierarchical' => true ),
	);
	register_taxonomy( 'partner-category', 'partner', $args );
}
add_action( 'init', 'create_partner_category_taxonomies', 0 );




// move metaboxes
function move_meta_box_partner(){
	remove_meta_box( 'postimagediv', 'partner', 'side' );
	add_meta_box('postimagediv', __('Featured Image'), 'post_thumbnail_meta_box', 'partner', 'normal', 'high');
}
add_action('do_meta_boxes', 'move_meta_box_partner');



// ACF date
if(function_exists('acf_add_local_field_group')) {
	acf_add_local_field_group(array(
		'key' => 'meta-partner-url',
		'title' => __('Odkaz','theme'),
		'position' => 'acf_after_title',
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'partner',
				),
			),
		),
		'fields' => array (
			array (
				'key' => 'partner_url',
				'name' => 'partner_url',
				'label' => '',
				'type' => 'url',
			),
		),
	));
}
