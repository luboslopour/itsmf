<?php

require_once dirname( __FILE__ ) . '/' . '../../../../wp-load.php';

if(current_user_can('edit_pages')):
?><!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Anketa spokojenosti <small><br><?php echo get_the_title($_GET['id']); ?></title>
		<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.min.js"></script>


	</head>
	<body>

		<script>
			var chartColors = [
				'#6f42c1',
				'#e83e8c',
				'#dc3545',
				'#fd7e14',
				'#ffc107',
				'#28a745',
				'#20c997',
				'#17a2b8',
				'#007bff',
				'#6610f2',
			];
		</script>





		<div class="container py-5">
			<h1>Anketa spokojenosti <small class="text-muted"><br><?php echo get_the_title($_GET['id']); ?></small></h1>
			<hr>
			<p class="lead text-muted">Počet hlasujících: <?php echo count(get_field('poll',$_GET['id'])); ?></p>



			<?php
				$questions = range(1,9);
				foreach ($questions as $question) {
					$q[$question] = [];
				}
				if(have_rows('poll',$_GET['id'])) {
					while(have_rows('poll',$_GET['id'])) {
						the_row();
						foreach ($questions as $question) {
							array_push($q[$question],get_sub_field('poll_'.$question));
							$title[$question] = get_sub_field_object('poll_'.$question)['label'];
							$choices[$question] = get_sub_field_object('poll_'.$question)['choices'];
						}
					}
				}
				foreach ($questions as $question) {
					$q[$question] = array_count_values($q[$question]);
				}
			?>

			<?php foreach ($questions as $question): ?>
				<div class="row align-items-center justify-content-center">
					<div class="col-lg-6 py-5">
						<h2><?php echo $title[$question]; ?></h2>
						<?php
							foreach ($choices[$question] as $key => $val) {
								$label[$key] = $val;
							}
							foreach ($q[$question] as $key => $val) {
								if($key==1) echo '<div class="lead"><strong>'.$label[1].'</strong>: '.$val.'&times;</div>';
								if($key==2) echo '<div class="lead"><strong>'.$label[2].'</strong>: '.$val.'&times;</div>';
								if($key==3) echo '<div class="lead"><strong>'.$label[3].'</strong>: '.$val.'&times;</div>';
							}
						?>
					</div>
					<div class="col-lg-6 py-5">
						<canvas id="chart-area<?php echo $question; ?>"></canvas>
					</div>
				</div>
				<hr>
			<?php endforeach; ?>


			<?php
				if(have_rows('poll',$_GET['id'])) {
					echo '<h2 class="mt-5 mb-5 pt-5">'.get_field_object('poll_topics')['label'].'</h2>';
					while(have_rows('poll',$_GET['id'])) {
						the_row();
						if(get_sub_field('poll_topics')) echo '<div class="bg-light p-4 mb-3">'.get_sub_field('poll_topics').'</div>';
					}
				}
			?>

			<?php
				if(have_rows('poll',$_GET['id'])) {
					echo '<h2 class="mt-5 mb-5 pt-5">'.get_field_object('poll_message')['label'].'</h2>';
					while(have_rows('poll',$_GET['id'])) {
						the_row();
						if(get_sub_field('poll_message')) echo '<div class="bg-light p-4 mb-3">'.get_sub_field('poll_message').'</div>';
					}
				}
			?>
		</div>

		<script>
			<?php foreach ($questions as $question): ?>

				var config<?php echo $question; ?> = {
					type: 'pie',
					data: {
						datasets: [{
							data: [
							<?php
								foreach ($q[$question] as $key => $val) {
									if($key==1) echo $val.',';
									if($key==2) echo $val.',';
									if($key==3) echo $val.',';
								}
							?>
							],
							backgroundColor: chartColors,
						}],
						labels: [
						<?php
							foreach ($q[$question] as $key => $val) {
								if($key==1) echo "'Velmi',";
								if($key==2) echo "'Spíše ano',";
								if($key==3) echo "'Spíše ne',";
							}
						?>
						]
					},
					options: {
						responsive: true
					}
				};
			<?php endforeach; ?>
			window.onload = function() {
				<?php foreach ($questions as $question): ?>
					chartColors.sort(function() {
						return .5 - Math.random();
					});
					var ctx<?php echo $question; ?> = document.getElementById('chart-area<?php echo $question; ?>').getContext('2d');
					window.myPie = new Chart(ctx<?php echo $question; ?>, config<?php echo $question; ?>);
				<?php endforeach; ?>
			};
		</script>
	</body>
</html><?php
else:
	wp_safe_redirect(admin_url());
	exit;
endif;