<?php get_header(); ?>
<?php if(have_posts()): ?>
	<div class="py-3">
		<div class="container">
			<div class="row">
				<div class="col-lg-9">
					<?php while(have_posts()): the_post(); ?>
						<div class="<?php if(is_archive()) echo 'bg-light p-3 '; ?>mb-3">
						<?php if(is_archive()): ?>
							<h2 class="h4"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h2>
						<?php endif; ?>
							<?php if(!is_archive()) the_category(); ?>
							<?php the_content() ?>
							<?php if(!is_archive()) the_tags(); ?>
						</div>
					<?php endwhile; ?>
				</div>
				<div class="col-lg-3">
					<?php get_template_part('sidebar') ?>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
<?php get_footer(); ?>
