<?php get_header(); ?>
<div class="py-3">
	<div class="container">
		<?php
			query_posts(array(
				'post_type' => 'events',
				'posts_per_page' => -1,
				'meta_key'  => 'date_from',
				'orderby'   => 'meta_value_num',
				'order'	 => 'DESC',
				'meta_query'	=> array(
					array(
						'key'	  	=> 'date_from',
						'value'	  	=> date('Ymd'),
						'type'	  => 'DATE',
						'compare' 	=> '>=',
					),
				),
			));
		?>
		<h2 class="h4"><?php _e('Nejbližší akce',get_template()) ?></h2>
		<?php if(have_posts()): ?>
			<table class="table table-striped table-responsive-md mb-5">
				<?php while(have_posts()): the_post(); ?>
					<tr>
						<td class="w-25">
							<?php require('template-parts/events-date.php') ?>
							<small><?php echo $date ?></small>
						</td>
						<td>
							<a class="text-primary" href="<?php the_permalink() ?>"><?php the_title() ?></a>
						</td>
					</tr>
				<?php endwhile; ?>
			</table>
		<?php else: ?>
			<p><?php _e('Žádné nadcházející akce.',get_template()) ?></p>
			<br>
		<?php endif; ?>
		<?php wp_reset_query(); ?>
		<?php
			query_posts(array(
				'post_type' => 'events',
				'posts_per_page' => -1,
				'orderby'   => 'date',
				'order'	 => 'DESC',
				//'meta_key'  => 'date_from',
				//'orderby'   => 'meta_value_num',
				//'order'	 => 'DESC',
				/*
				'meta_query'	=> array(
					array(
						'key'	  	=> 'date_from',
						'value'	  	=> date('Ymd'),
						'type'	  => 'DATE',
						'compare' 	=> '<',
					),
				),
				*/
			));
		?>
		<h2 class="h4"><?php _e('Proběhlé akce',get_template()) ?></h2>
		<?php if(have_posts()): ?>
			<table class="table table-striped table-responsive-md">
				<?php while(have_posts()): the_post(); ?>
					<tr>
						<td class="w-25">
							<?php require('template-parts/events-date.php') ?>
							<small><?php echo $date ?></small>
						</td>
						<td>
							<a class="text-primary" href="<?php the_permalink() ?>"><?php the_title() ?></a>
						</td>
					</tr>
				<?php endwhile; ?>
			</table>
		<?php else: ?>
			<p><?php _e('Žádné proběhlé akce.',get_template()) ?></p>
			<br>
		<?php endif; ?>
		<?php wp_reset_query(); ?>
	</div>
</div>
<?php get_footer(); ?>
