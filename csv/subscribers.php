<?php
require('../../../../wp-load.php');
if(current_user_can('edit_pages')) {
	header('Content-Type: text/csv; charset=utf-8');
	header('Content-Disposition: attachment; filename=subscribers.csv');
	query_posts(array(
		'post_type' => 'subscribers',
		'posts_per_page' => -1,
		'orderby' => 'title',
		'order' => 'ASC',
	));
	$output = fopen('php://output','w');
	if(have_posts()) {
		echo 'Email Address,Last Name,First Name'."\n";
		while(have_posts()) {
			the_post();
			fputcsv($output,array(
				get_the_title(),
				get_field('lastname'),
				get_field('firstname'),
			));
		}
	}
} else {
	wp_safe_redirect(admin_url());
	exit;
}