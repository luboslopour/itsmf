<?php
	query_posts(array(
		'post_type' => 'events',
		'posts_per_page' => 3,
		'meta_key'  => 'date_from',
		'orderby'   => 'meta_value_num',
		'order'	 => 'DESC',

		/*
		'meta_query'	=> array(
			array(
				'key'	  	=> 'date_from',
				'value'	  	=> date('Ymd'),
				'type'	  => 'DATE',
				'compare' 	=> '>=',
			),
		),
		
		*/
	));
?>
<?php if(have_posts()): ?>
	<section>
		<h2 class="h4"><?php _e('Nejbližší akce',get_template()) ?></h2>
		<div class="w-25 border border-primary mt-3"></div>
		<?php while(have_posts()): the_post(); ?>
			<article class="border-bottom border-light pt-3 pb-3">
				<h3 class="h6 mb-0"><a class="text-primary" href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
				<p class="text-muted mb-0">
					<svg class="icon-material-calendar mb-1 mr-1">
						<use xlink:href="#icon-material-calendar"></use>
					</svg>
					<?php require('template-parts/events-date.php') ?>
					<small><?php echo $date ?></small>
				</p>
			</article>
		<?php endwhile; ?>
		<a href="<?php echo get_post_type_archive_link('events'); ?>" class="btn btn-primary mt-4">
			<?php _e('Všechny akce','theme') ?>
		</a>
	</section>
<?php endif; ?>
<?php wp_reset_query(); ?>
