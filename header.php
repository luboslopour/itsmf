<!doctype html>
<html <?php language_attributes() ?>>
	<head>
		<meta http-equiv="Content-Type" content="<?php bloginfo('html_type') ?>;charset=<?php bloginfo('charset') ?>">
		<meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
		
		<title><?php wp_title(' - ',true,'right'); bloginfo('name'); ?></title>

		<!-- favicon -->
		<link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('template_url') ?>/dist/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_url') ?>/dist/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('template_url') ?>/dist/favicon/favicon-16x16.png">
		<link rel="manifest" href="<?php bloginfo('template_url') ?>/dist/favicon/site.webmanifest">
		<link rel="mask-icon" href="<?php bloginfo('template_url') ?>/dist/favicon/safari-pinned-tab.svg" color="#5bbad5">
		<link rel="shortcut icon" href="<?php bloginfo('template_url') ?>/dist/favicon/favicon.ico">
		<meta name="msapplication-TileColor" content="#b91d47">
		<meta name="msapplication-config" content="<?php bloginfo('template_url') ?>/dist/favicon/browserconfig.xml">
		<meta name="theme-color" content="#ffffff">

<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
		<?php if($_GET['login']==='success'): ?>
			<div class="container py-4">
				<div class="bg-success text-white px-3 py-2">
					<?php _e('Jste příhlášen jako:',get_template()) ?>
					<a class="text-white" href="<?php echo get_edit_profile_url() ?>">
						<?php echo wp_get_current_user()->display_name; ?>
					</a>
					&bull;
					<a class="text-white" href="<?php echo wp_logout_url(get_the_permalink().'?logout=success') ?>">
						<?php _e('Odhlášení',get_template()) ?>.
					</a>
				</div>
			</div>
		<?php endif; ?>
		<?php if($_GET['logout']==='success'): ?>
			<div class="container py-4">
				<div class="bg-success text-white px-3 py-2">
					<?php _e('Odhlášení proběhlo úspěšně.',get_template()) ?>
				</div>
			</div>
		<?php endif; ?>
		<nav class="navbar navbar-light navbar-expand-xl bg-white py-3">
			<div class="container">
				<a class="navbar-brand" href="<?php bloginfo('url'); ?>">
					<img class="img-fluid" src="<?php bloginfo('template_url') ?>/dist/img/logo.svg" alt="<?php bloginfo('name'); ?>" width="200">
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainmenu" aria-controls="mainmenu" aria-expanded="false" aria-label="<?php _e('Hlavní menu',get_template()) ?>">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="mainmenu">
					<?php
						wp_nav_menu(
							array(
								'theme_location' => 'header',
								'container'      =>'',
								'walker'	     => new Understrap_WP_Bootstrap_Navwalker(),
								'fallback_cb'    => false,
								'items_wrap'     => '<ul class="navbar-nav mr-0 ml-auto">%3$s</ul>',
							)
						)
					?>
					<div class="border-left align-self-stretch mx-2"></div>
					<ul class="navbar-nav align-items-center">
						<?php
							wp_nav_menu(
								array(
									'theme_location' => 'lang',
									'container'      =>'',
									'walker'	     => new Understrap_WP_Bootstrap_Navwalker(),
									'fallback_cb'    => false,
									'items_wrap'     => '',
								)
							)
						?>
						<li class="nav-item d-flex align-items-center mr-2">
							<?php if(is_user_logged_in()): ?>
								<a href="<?php echo wp_logout_url(); ?> " class="nav-link d-flex align-items-center" title="<?php _e('Odhlášení',get_template()) ?>" data-toggle="tooltip">
									<svg class="icon-material-user" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32">
										<path d="M11.429 25.714c0 0.5 0.232 1.714-0.571 1.714h-5.714c-2.839 0-5.143-2.304-5.143-5.143v-12.571c0-2.839 2.304-5.143 5.143-5.143h5.714c0.304 0 0.571 0.268 0.571 0.571 0 0.5 0.232 1.714-0.571 1.714h-5.714c-1.571 0-2.857 1.286-2.857 2.857v12.571c0 1.571 1.286 2.857 2.857 2.857h5.143c0.446 0 1.143-0.089 1.143 0.571zM28 16c0 0.304-0.125 0.589-0.339 0.804l-9.714 9.714c-0.214 0.214-0.5 0.339-0.804 0.339-0.625 0-1.143-0.518-1.143-1.143v-5.143h-8c-0.625 0-1.143-0.518-1.143-1.143v-6.857c0-0.625 0.518-1.143 1.143-1.143h8v-5.143c0-0.625 0.518-1.143 1.143-1.143 0.304 0 0.589 0.125 0.804 0.339l9.714 9.714c0.214 0.214 0.339 0.5 0.339 0.804z"></path>
									</svg>
									<span class="sr-only"><?php _e('Odhlášení',get_template()) ?></span>
								</a>
							<?php else: ?>
								<a href="<?php echo wp_login_url(get_the_permalink().'/?login=success'); ?> " class="nav-link d-flex align-items-center" title="<?php _e('Přihlášení',get_template()) ?>" data-toggle="tooltip">
									<svg class="icon-material-user" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32">
										<path d="M16 20.023q5.053 0 10.526 2.199t5.474 5.754v4.023h-32v-4.023q0-3.556 5.474-5.754t10.526-2.199zM16 16q-3.275 0-5.614-2.339t-2.339-5.614 2.339-5.661 5.614-2.386 5.614 2.386 2.339 5.661-2.339 5.614-5.614 2.339z"></path>
									</svg>
									<span class="sr-only"><?php _e('Přihlášení',get_template()) ?></span>
								</a>
							<?php endif; ?>
						</li>
					</ul>
				</div>
			</div>
		</nav>

		<header>
			<?php if(!is_front_page()): ?>
				<div class="bg-light skewed-arrow-down pb-4">
					<div class="bg-primary skewed-arrow-down text-white text-center py-5">
						<div class="container pb-4">
							<h1><?php
								if(is_archive()) {
									the_archive_title();
								} elseif(is_404()) {
									_e('Chyba 404! Stránka nenalezena',get_template());
								} else {
									the_title();
								}
							?></h1>
						</div>
					</div>
				</div>
			<?php else: ?>
				<h1 class="sr-only"><?php the_title() ?></h1>
			<?php endif; ?>
			<?php if(function_exists('bcn_display') && !is_front_page()): ?>
				<div class="small py-3">
					<div class="container">
						<?php bcn_display(); ?>
					</div>
				</div>
			<?php endif; ?>
		</header>

		<main>