// --- PLUGINS ---
var gulp = require('gulp');
var clean = require('gulp-clean');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var watch = require('gulp-watch');
var image = require('gulp-image');
var nunjucks = require('gulp-nunjucks');
var htmlbeautify = require('gulp-html-beautify');
var livereload = require('gulp-livereload');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var fs = require('fs');




// --- CLEAN ---
gulp.task('clean', function(done) {
	gulp.src('dist')
		.pipe(clean());
	done();
});



// --- COPY ---
gulp.task('copy', function(done){
	gulp.src(['node_modules/popper.js/dist/**/*'])
		.pipe(gulp.dest('dist/popper.js/'));
	gulp.src(['node_modules/jquery/dist/**/*'])
		.pipe(gulp.dest('dist/jquery/'));
	gulp.src(['src/favicon/**/*'])
		.pipe(gulp.dest('dist/favicon/'));
	done();
});



// --- CSS ---
gulp.task('css', function(done) {
	gulp.src('src/scss/main.scss')
	.pipe(plumber({
		errorHandler: function(err) {
			notify.onError()(err);
		}
	}))
		//.pipe(sourcemaps.init())
		.pipe(sass())
		.pipe(autoprefixer())
		.pipe(cleanCSS({
			rebase: false,
			level: 2
		}))
		.pipe(concat('style.min.css'))
		//.pipe(sourcemaps.write(''))
		.pipe(gulp.dest('dist/css/'));

	done();
});



// --- JS ---
var sourceJS = require('./src/js/main.json');
gulp.task('js', function(done) {
	gulp.src(sourceJS)
	.pipe(plumber({
		errorHandler: function(err) {
			notify.onError()(err);
		}
	}))
		//.pipe(sourcemaps.init())
		.pipe(concat('scripts.min.js'))
		.pipe(uglify())
		//.pipe(sourcemaps.write(''))
		.pipe(gulp.dest('dist/js/'));
	done();
});



// --- IMAGES ---
gulp.task('images', function(done) {
	gulp.src('src/img/**/*')
		.pipe(image())
		.pipe(gulp.dest('dist/img'));
	done();
});



// --- HTML ---
gulp.task('templates', function(done) {
	gulp.src('src/templates/**.html')
	.pipe(plumber({
		errorHandler: function(err) {
			notify.onError()(err);
		}
	}))
	.pipe(nunjucks.compile({
		currentTime: new Date().getTime()
	}))
	.pipe(htmlbeautify({
		'indent_with_tabs': true,
		'max_preserve_newlines': 1,
	}))
	.pipe(gulp.dest('dist/templates'));
	done();
});



// --- README ---
gulp.task('readme', function(done) {
	var json = JSON.parse(fs.readFileSync('package.json'));
	var devDependencies = [];
	var dependencies = [];
	for (var item in json.devDependencies){
		var itemjson = JSON.parse(fs.readFileSync('node_modules/'+item+'/package.json'));
		devDependencies.push({'name':itemjson.name,'description':itemjson.description});
	}
	for (var item in json.dependencies){
		var itemjson = JSON.parse(fs.readFileSync('node_modules/'+item+'/package.json'));
		dependencies.push({'name':itemjson.name,'description':itemjson.description});
	}
	gulp.src('src/readme/README.md')
		.pipe(nunjucks.compile({
			'package':json,
			'devDependencies':devDependencies,
			'dependencies':dependencies
		}))
		.pipe(gulp.dest('./'));
	done();
});



// --- DEFAULT ---
gulp.task('default', gulp.parallel('copy', 'css', 'js', 'images', 'templates', 'readme'));



// --- WATCH ---
gulp.task('watch',function(done){
	gulp.watch(['src/scss/**/*.scss'],gulp.series('css'));
	gulp.watch(['src/js/**/*.js'],gulp.series('js'));
	gulp.watch(['src/templates/**/*.html'],gulp.series('templates'));
	livereload.listen();
	gulp.watch([
		'dist/css/*.css',
		'dist/js/*.js',
		'dist/templates/*.html',
		'**/*.php'
	]).on('change',livereload.changed);
	done();
});