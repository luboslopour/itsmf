<?php

if ( function_exists( 'acf_add_options_page' ) ) {
	$parent = acf_add_options_page(array(
		'page_title' => _x('Kontakty','admin',get_template()),
		'menu_title' => _x('Kontakty','admin',get_template()),
		'capability' => 'edit_pages',
		'icon_url'   => 'dashicons-id',
	));

	$languages = get_available_languages();
	if(!$languages) {
		$languages = array(get_locale());
	}

	foreach ($languages as $lang) {
		$langcode = '';
		if(count($languages)>1) {
			$langcode = ' ('.strtoupper(substr($lang,0,2)).'/'.strtoupper(substr($lang,3,2)).')';
		}
		acf_add_options_sub_page( array(
			'page_title' => _x('Kontakty','admin',get_template()).$langcode,
			'menu_title' => _x('Kontakty','admin',get_template()).$langcode,
			'menu_slug'  => 'contact-settings-'.$lang,
			'parent'     => $parent['menu_slug']
		));

		acf_add_local_field_group(array(
			'key' => 'meta-option-contacts-'.$lang,
			'title' => _x('Kontakty','admin',get_template()).$langcode,
			'label_placement' => 'left',
			'location' => array (
				array (
					array (
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'contact-settings-'.$lang,
					),
				),
			),
			'fields' => array (
				array (
					'key' => 'option_email-'.$lang,
					'name' => 'option_email-'.$lang,
					'label' => _x('Email','admin',get_template()),
					'type' => 'email',
				),
			),
		));
	}
}
