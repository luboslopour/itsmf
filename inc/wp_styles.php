<?php

function wp_theme_styles() {
	wp_enqueue_style(get_template(),get_bloginfo('template_url').'/dist/css/style.min.css',array(),filemtime(get_theme_file_path('/dist/css/style.min.css')),'all');
}
add_action('wp_enqueue_scripts','wp_theme_styles');