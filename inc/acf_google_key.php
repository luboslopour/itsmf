<?php

function acf_google_key() {
	acf_update_setting('google_api_key',get_field('option_google_maps_key-'.get_locale(),'option'));
}
add_action('acf/init', 'acf_google_key');