<?php

function excerpt_custom_continue( $more ) {
	return '&hellip;';
}
add_filter('excerpt_more', 'excerpt_custom_continue');
