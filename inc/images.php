<?php


// update image sizes
update_option('medium_large_size_w','0');
update_option('medium_large_size_h','0');

update_option( 'thumbnail_size_w', 350 );
update_option( 'thumbnail_size_h', 350 );
update_option( 'thumbnail_crop', 1 );

update_option( 'medium_size_w', 540 );
update_option( 'medium_size_h', 0 );

update_option( 'large_size_w', 1110 );
update_option( 'large_size_h', 0 );

/*
// add image size
add_image_size('thumbnail-360x360',360,360,true);

// image sizes name
function custom_sizes_name($sizes) {
	return array_merge( $sizes, array(
		'thumbnail-360x360' => _x('Thumbnail (Large)','admin',get_template()),
	));
}
add_filter('image_size_names_choose','custom_sizes_name');
*/



// remove special characters from uploaded media
function sanitize_media ($filename) {
	return remove_accents($filename);
}
add_filter('sanitize_file_name', 'sanitize_media', 10);



// jpg quality (sharpen generated images JPG)
function wps_sharpen_resized_file( $resized_file ) {
	$image = wp_load_image( $resized_file );
	if ( !is_resource( $image ) )
		return new WP_Error( 'error_loading_image', $image, $file );
	$size = @getimagesize( $resized_file );
	if ( !$size )
		return new WP_Error('invalid_image', __('Could not read image size'), $file);
	list($orig_w, $orig_h, $orig_type) = $size;
	switch ( $orig_type ) {
		case IMAGETYPE_JPEG:
			$matrix = array(
				array(-1, -1, -1),
				array(-1, 16, -1),
				array(-1, -1, -1),
			);
			$divisor = array_sum(array_map('array_sum', $matrix));
			$offset   = 0; 
			imageconvolution($image, $matrix, $divisor, $offset);
			imagejpeg($image, $resized_file,apply_filters( 'jpeg_quality', 90, 'edit_image' ));
			break;
		case IMAGETYPE_PNG:
			return $resized_file;
		case IMAGETYPE_GIF:
			return $resized_file;
	}
 
	return $resized_file;
}   
add_filter('image_make_intermediate_size', 'wps_sharpen_resized_file',900);