<?php

// remove post type
function unregister_post_types() {
	global $wp_post_types;
	if (isset($wp_post_types['post'])) {
		unset( $wp_post_types['post']);
		return true;
	}
	return false;
}
add_action('init','unregister_post_types');