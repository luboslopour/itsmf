<?php

if(function_exists('bcn_display')) {
	function my_bcn_allowed_html($allowed_html){
		$allowed_html['li'] = array(
			'title' => true,
			'class' => true,
			'id' => true,
			'dir' => true,
			'align' => true,
			'lang' => true,
			'xml:lang' => true,
			'aria-hidden' => true,
			'data-icon' => true,
			'itemref' => true,
			'itemid' => true,
			'itemprop' => true,
			'itemscope' => true,
			'itemtype' => true
		);
		$allowed_html['svg'] = array(
			'version' => true,
			'xmlns' => true,
			'viewbox' => true,
			'viewBox' => true,
			'title' => true,
			'class' => true,
			'id' => true
		);
		$allowed_html['path'] = array(
			'd' => true
		);
		return $allowed_html;
	}
	add_filter('bcn_allowed_html', 'my_bcn_allowed_html');
}

