<?php

// YOAST SEO metabox position
function yoast_seo_metabox() {
	return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoast_seo_metabox');