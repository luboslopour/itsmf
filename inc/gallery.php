<?php


// remove [gallery] inline style
add_filter('use_default_gallery_style', '__return_false');



// deafaults gallery
function wp_gallery_default( $settings ) {
	$settings['galleryDefaults']['link'] = 'file';
	$settings['galleryDefaults']['columns'] = 5;
	return $settings;
}
add_filter( 'media_view_settings', 'wp_gallery_default');




// valid HTML5 | gallery shortcode
function wp_gallery_shortcode($content){
    return str_replace('[gallery', '[gallery gallerytag="span" itemtag="span" icontag="span" captiontag="span"', $content);
}
add_filter('the_content', 'wp_gallery_shortcode');
