<?php



// table attributes
function table_border($content) {
	$content = str_replace('alignleft', 'alignleft float-left mr-3 my-2 mw-50 img-fluid', $content);
	$content = str_replace('aligncenter', 'aligncenter d-block mx-auto my-2 img-fluid', $content);
	$content = str_replace('alignright', 'alignright float-right ml-3 my-2 mw-50 img-fluid', $content);
	$content = str_replace('alignnone', 'alignnone d-inline-block align-text-bottom img-fluid', $content);
	$content = str_replace('<blockquote', '<blockquote class="blockquote"', $content);

	return $content;
}
add_filter('the_content', 'table_border');
add_filter('acf/format_value/type=wysiwyg', 'table_border');




function wrap_embed_with_div($html, $url, $attr) {
     return '<div class="embed-responsive embed-responsive-21by9 mb-3">' . $html . '</div>';
}

 add_filter('embed_oembed_html', 'wrap_embed_with_div', 10, 3);