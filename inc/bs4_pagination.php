<?php
/**!
 * Bootstrap pagination for index and category pages
 */

if ( ! function_exists( 'b4st_pagination' ) ) {
	function b4st_pagination() {
		global $wp_query;

		$big = 999999999; // need an unlikely integer
		$paginate_links = paginate_links(
			array(
				'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'format' => '?paged=%#%',
				'mid_size' => 1,
				'current' => max( 1, get_query_var('paged') ),
				'total' => $wp_query->max_num_pages,
				'type' => 'list'
			)
		);

		$paginate_links = str_replace( "<ul class='page-numbers'>", "<ul class=\"pagination justify-content-center flex-wrap d-print-none\">", $paginate_links );
		$paginate_links = str_replace( "<li", "<li class=\"page-item\"", $paginate_links );
		$paginate_links = str_replace( "prev page-numbers", "prev page-link", $paginate_links );
		$paginate_links = str_replace( "next page-numbers", "next page-link", $paginate_links );
		$paginate_links = str_replace( "'page-numbers'", "\"page-link\"", $paginate_links );
		$paginate_links = str_replace( '<li class="page-item"><a class="prev ', '<li class="page-item d-none d-md-block"><a class="', $paginate_links );
		$paginate_links = str_replace( '<li class="page-item"><a class="next ', '<li class="page-item d-none d-md-block"><a class="', $paginate_links );

		$paginate_links = str_replace( '<li class="page-item"><span class="page-numbers dots">', '<li class="page-item disabled"><span class="page-link">', $paginate_links );
		$paginate_links = str_replace( "<span aria-current='page' class='page-numbers current'>", '<span class="page-link current">', $paginate_links );
		$paginate_links = str_replace( '<li class="page-item"><span class="page-link current">', '<li class="page-item disabled"><span class="page-link bg-light">', $paginate_links );
		if ($paginate_links) {
			echo $paginate_links;
		}
	}
}