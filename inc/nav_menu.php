<?php

// allow custom menu
add_theme_support('menus');



// allow editor manage custom menu
get_role('editor')->add_cap( 'edit_theme_options' );



// register menus
function register_theme_menus() {
	register_nav_menus(
		array(
			'top' => _x('Horní menu','admin',get_template()),
			'header' => _x('Hlavní menu','admin',get_template()),
			'footer' => _x('Menu zápatí','admin',get_template()),
		)
	);
}
add_action( 'init', 'register_theme_menus' );



// Limit max menu depth in admin panel to 2 (1 submenu)
function q242068_limit_depth( $hook ) {
  if ( $hook != 'nav-menus.php' ) return;
  wp_add_inline_script( 'nav-menu', 'wpNavMenu.options.globalMaxDepth = 1;', 'after' );
}
add_action( 'admin_enqueue_scripts', 'q242068_limit_depth' );