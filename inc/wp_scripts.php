<?php

function wp_theme_scripts() {
	wp_deregister_script('jquery');
	wp_deregister_script('wp-embed');

	wp_enqueue_script('popper.js',get_bloginfo('template_url').'/dist/popper.js/umd/popper.min.js',array(),filemtime(get_theme_file_path('/dist/popper.js/umd/popper.min.js')),false);
	wp_enqueue_script('jquery',get_bloginfo('template_url').'/dist/jquery/jquery.min.js',array('popper.js'),filemtime(get_theme_file_path('/dist/jquery/jquery.min.js')),false);
	wp_enqueue_script('scripts',get_bloginfo('template_url').'/dist/js/scripts.min.js',array('jquery'),filemtime(get_theme_file_path('/dist/js/scripts.min.js')),true);

}
add_action('wp_enqueue_scripts','wp_theme_scripts');


// remove script and style attripute type
function remove_script_type_attr($tag, $handle) {
	return preg_replace( "/ type=['\"]text\/(javascript|css)['\"]/", '', $tag );
}
add_filter('script_loader_tag', 'remove_script_type_attr', 10, 2);
add_filter('style_loader_tag', 'remove_script_type_attr', 10, 2);
