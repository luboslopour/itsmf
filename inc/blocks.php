<?php

// align wide
function block_theme_setup() {
	add_theme_support('align-wide');
}
add_action( 'after_setup_theme', 'block_theme_setup' );