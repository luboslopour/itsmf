<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
	<div class="py-5">
		<div class="container">
			<div class="row">
				<div class="col-lg-9">
					<?php the_content() ?>
				</div>
				<div class="col-lg-3">
					<?php get_template_part('sidebar') ?>
				</div>
			</div>
		</div>
	</div>
<?php endwhile; ?>
<?php get_footer(); ?>
