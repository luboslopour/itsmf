<?php get_header(); ?>
	<div class="container py-5">
		<h1><?php _e('Vyhledávání',get_template()); ?></h1>
		<hr>
		<?php get_search_form(); ?>
		<hr>
		<?php if(!empty($_GET['s'])) : ?>
			<h2><?php printf(__('Hledaný výraz: %s',get_template()), get_search_query() ); ?></h2>
			<?php if(have_posts()): ?>
			<?php
				while ( have_posts() ) : the_post(); ?>
				<h2>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					<small>- <?php echo get_post_type_object(get_post_type(get_the_ID()))->labels->name; ?></small>
				</h2>
				<?php the_excerpt(); ?>
			<?php endwhile; ?>
			<?php if(has_next_posts or has_previous_posts): ?>
				<div class="d-flex">
					<?php posts_nav_link(' ','<span class="btn btn-outline-primary m-3">'.__('&laquo; Previous').'</span>','<span class="btn btn-outline-primary m-3">'.__('Next &raquo;').'</span>'); ?>
				</div>
			<?php endif; ?>
			<?php else: ?>
				<p class="lead"><?php _e('Nebylo nic nalezeno',get_template()); ?></p>
			<?php endif; ?>
		<?php endif; ?>
	</div>
<?php get_footer(); ?>