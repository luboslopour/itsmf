# ITSMF
Theme for WordPress.
 **Version**: 1.0.0

## Installation
Run `$ npm install` in root direcory.

## Contributors
Luboš Lopour, [lubos.lopour@gmail.com](mailto:lubos.lopour@gmail.com)
## Scripts
Convert all:  `$ gulp default`
Build css:  `$ gulp css`
Build js:  `$ gulp js`
Convert images:  `$ gulp images`
Build templates:  `$ gulp templates`
Build readme:  `$ gulp readme`
Watch mode + livereload:  `$ gulp watch`
Clean:  `$ gulp clean`
## Dev Dependencies
| Name | Description |
| --- | --- |
| [fs](https://ghub.io/fs) |  This package name is not currently in use, but was formerly occupied by another package. To avoid malicious use, npm is hanging on to the package name, but loosely, and we&#39;ll probably give it to you if you want it. |
| [gulp](https://ghub.io/gulp) |  The streaming build system. |
| [gulp-clean](https://ghub.io/gulp-clean) |  A gulp plugin for removing files and folders. |
| [gulp-sass](https://ghub.io/gulp-sass) |  Gulp plugin for sass |
| [gulp-clean-css](https://ghub.io/gulp-clean-css) |  Minify css with clean-css. |
| [gulp-autoprefixer](https://ghub.io/gulp-autoprefixer) |  Prefix CSS |
| [gulp-concat](https://ghub.io/gulp-concat) |  Concatenates files |
| [gulp-sourcemaps](https://ghub.io/gulp-sourcemaps) |  Source map support for Gulp.js |
| [gulp-uglify](https://ghub.io/gulp-uglify) |  Minify files with UglifyJS. |
| [gulp-watch](https://ghub.io/gulp-watch) |  Watch, that actually is an endless stream |
| [gulp-image](https://ghub.io/gulp-image) |  Optimize PNG, JPG, GIF, SVG images with gulp task. |
| [gulp-nunjucks](https://ghub.io/gulp-nunjucks) |  Compile/precompile Nunjucks templates |
| [gulp-html-beautify](https://ghub.io/gulp-html-beautify) |  This is a gulp plugin to beautify HTML files. |
| [gulp-livereload](https://ghub.io/gulp-livereload) |  Gulp plugin for livereload. |
| [gulp-notify](https://ghub.io/gulp-notify) |  gulp plugin to send messages based on Vinyl Files or Errors to Mac OS X, Linux or Windows using the node-notifier module. Fallbacks to Growl or simply logging |
| [gulp-plumber](https://ghub.io/gulp-plumber) |  Prevent pipe breaking caused by errors from gulp plugins |
| [popper.js](https://ghub.io/popper.js) |  A kickass library to manage your poppers |
| [jquery](https://ghub.io/jquery) |  JavaScript library for DOM operations |
| [bootstrap](https://ghub.io/bootstrap) |  The most popular front-end framework for developing responsive, mobile first projects on the web. |
| [owl.carousel](https://ghub.io/owl.carousel) |  Touch enabled jQuery plugin that lets you create beautiful responsive carousel slider. |
| [@fancyapps/fancybox](https://ghub.io/@fancyapps/fancybox) |  Touch enabled, responsive and fully customizable jQuery lightbox script |
| [jquery-viewport-checker](https://ghub.io/jquery-viewport-checker) |  Little script that detects if an element is in the viewport and adds a class to it. |
