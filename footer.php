		</main>
		
		<aside>

			<?php
				$args = array(
					'taxonomy' => array('partner-category'),
				);
				$partners = get_terms($args);
			?>
			<?php if ($partners): ?>
				<div class="bg-light pt-5 pb-2">
					<div class="container">
						<h2><?php _e('Partneři konference itSMF',get_template()) ?></h2>
						<div class="row">
							<div class="partners-slider">
								<?php foreach ($partners as $partner): ?>
									<div class="item">
										<div class="col-12">
											<div class="row py-2">
												<div class="col-lg-3 d-flex">
													<div class="bg-white d-flex justify-content-center align-items-center w-100 p-3">
														<h2 class="h5 mb-0"><?php echo $partner->name ?></h2>
													</div>
												</div>
												<?php
													query_posts(array(
														'post_type' => 'partner',
														'posts_per_page' => -1,
														'orderby' => 'menu_order title',
														'order'		=> 'ASC',
														'tax_query' => array(
														    'relation' => 'AND',
														    array(
														        'taxonomy' => 'partner-category',
														        'field'    => 'term_id',
														        'terms'    => array($partner->term_id),
														    ),
														),
													));
												?>
												<?php if(have_posts()) : ?>
													<div class="col-lg-9 d-flex">
														<div class="logo-list bg-white w-100 p-3 d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
															<?php while(have_posts()) : the_post(); ?>
																<?php if(get_field('partners_url')) echo '<a target="_blank" href="'.get_field('partners_url').'">'; ?>
																	<span class="mx-3 my-2">
																		<?php if(has_post_thumbnail()): ?>
																			<?php the_post_thumbnail('medium',array('class'=>'img-fluid')); ?>
																		<?php else: ?>
																			<?php the_title() ?>
																		<?php endif; ?>
																	</span>
																<?php if(get_field('partners_url')) echo '</a>'; ?>
															<?php endwhile; ?>
														</div>
													</div>
												<?php endif; ?>
												<?php wp_reset_query(); ?>
											</div>
										</div>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</aside>

		<footer>
			<div class="bg-light">
				<form>
					<div class="embed-responsive bg-dark text-white skewed-arrow-up py-5">
						<img class="embed-responsive-item opacity-25" src="<?php bloginfo('template_url') ?>/dist/img/example/apple-1851464_1920.jpg" alt="">
						<div class="container position-relative">
							<div class="text-center">
								<svg class="icon-material-mail opacity-75 display-2 mb-3">
									<use xlink:href="#icon-material-mail"></use>
								</svg>
								<div>
									<h2><?php _e('Registrace k odběru novinek',get_template()) ?></h2>
									<p class="lead">
										<?php _e('Chcete být pravidelně informováni o akcích a novinkách itSMF Czech Republic?',get_template()) ?>
										<br>
										<?php _e('Neváhejte a zanechte nám svůj e-mail.',get_template()) ?>
									</p>
									<div class="row justify-content-center">
										<div class="col-sm-9 col-md-7 col-lg-5">
											<div class="input-group mb-4">
												<input type="email" class="form-control border-white" id="inputPassword2" placeholder="Váš E-mail" aria-label="Váš E-mail">
												<div class="input-group-append">
													<button class="btn btn-primary" type="submit">Přihlásit</button>
												</div>
											</div>
											<div class="small">
												<p>
													<?php _e('Odesláním formuláře souhlasíte se zpracováním osobních údajů.',get_template()) ?>
													<br><a class="text-light" href="<?php echo get_the_permalink(get_option('wp_page_for_privacy_policy')) ?>">Zásady ochrany osobních údajů</a>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>

			<div class="bg-dark text-white py-5">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="small">
								<p>
									<a href="#"><img class="img-fluid" src="<?php bloginfo('template_url') ?>/dist/img/logo-white.svg" width="220" alt="<?php bloginfo('name') ?>"></a>
								</p>
								<p>itSMF (IT Service Management Forum) je mezinárodně působící nezávislá a nezisková organizace účelově se věnující všem aspektům řízení služeb informačních a komunikačních technologií. Celosvětovým standardem pro tuto oblast je ITIL a z něj vycházející norma ISO/IEC 20000, itSMF je tedy současně vnímáno jako fórum uživatelů tohoto standardu, které ale současně zásadním způsobem ovlivňuje rozvoj celého odvětví ICT managementu.</p>
								<p>&copy; <?php $y=date('Y'); if ($y>2010) { echo'2010 - '.$y; } else { echo $y; } ?> | <a class="text-light" href="<?php bloginfo('url'); ?>">itSMF Czech Republic</a> Všechna práva vyhrazena.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>

		<svg class="sr-only">
			<symbol id="icon-material-calendar" viewBox="0 0 32 32">
				<path d="M27.643 29.072v-18.86h-23.285v18.86h23.285zM27.643 2.928q1.157 0 2.009 0.885t0.851 2.043v23.217q0 1.157-0.851 2.043t-2.009 0.885h-23.285q-1.157 0-2.009-0.885t-0.851-2.043v-23.217q0-1.157 0.851-2.043t2.009-0.885h1.498v-2.928h2.86v2.928h14.57v-2.928h2.86v2.928h1.498z"></path>
			</symbol>
			<symbol id="icon-material-mail" viewBox="0 0 32 32">
				<path d="M16 14.423l12.845-8.038h-25.69zM28.845 25.615v-16l-12.845 7.962-12.845-7.962v16h25.69zM28.845 3.155q1.277 0 2.216 0.977t0.939 2.254v19.23q0 1.277-0.939 2.254t-2.216 0.977h-25.69q-1.277 0-2.216-0.977t-0.939-2.254v-19.23q0-1.277 0.939-2.254t2.216-0.977h25.69z"></path>
			</symbol>
		</svg>
		
		<?php wp_footer() ?>
	</body>
</html>